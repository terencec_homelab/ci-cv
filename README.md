# CI/CV

Mon CV rédigé en Latex hébergé sur Gitlab Pages grâce à Gitlab CI.

Il est visible [ici](https://terencec_homelab.gitlab.io/ci-cv).
